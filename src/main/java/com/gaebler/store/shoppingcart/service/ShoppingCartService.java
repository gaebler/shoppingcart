package com.gaebler.store.shoppingcart.service;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gaebler.store.shoppingcart.domain.CommerceItem;
import com.gaebler.store.shoppingcart.domain.Product;
import com.gaebler.store.shoppingcart.domain.ShoppingCart;
import com.gaebler.store.shoppingcart.repository.CommerceItemRepository;
import com.gaebler.store.shoppingcart.repository.ProductRepository;
import com.gaebler.store.shoppingcart.repository.ShoppingCartRepository;

@Service
@Transactional
public class ShoppingCartService {

    @Inject
    private ShoppingCartRepository shoppingCartRepository;
    
    @Inject
    private ProductRepository productRepository;
    
    @Inject
    private CommerceItemRepository itemRepository;
    
    public ShoppingCart getShoppingCart(String shoppingCartId){
    	ShoppingCart shoppingCart = shoppingCartRepository.findOne(shoppingCartId);
    	if(shoppingCart==null){
    		shoppingCart = new ShoppingCart(shoppingCartId);
    		shoppingCartRepository.save(shoppingCart);
    	}
    	return shoppingCart;
    }
    
    public CommerceItem addItem(String shoppingCartId, String productId, Integer quantity){
    	ShoppingCart shoppingCart = getShoppingCart(shoppingCartId);
    	Product product = productRepository.findOne(productId);
    	BigDecimal amountAdded = product.getPrice().multiply(new BigDecimal(quantity));
    	
    	CommerceItem item = itemRepository.findByShoppingCartIdAndProductId(shoppingCartId, productId);
    	item = item==null?new CommerceItem():item;	
    	item.setShoppingCart(shoppingCart);
    	item.setQuantity(Integer.sum(item.getQuantity(), quantity));
    	item.setProduct(product);
    	item.setAmount(item.getAmount().add(amountAdded));
    	itemRepository.save(item);
    	
    	shoppingCart.setAmount(shoppingCart.getAmount().add(amountAdded));
    	shoppingCart.getItems().add(item);
    	
    	return item;
    }
    
    public void removeItem(String commerceItemId){
    	CommerceItem item = itemRepository.findOne(commerceItemId);
    	if(item!=null){
    		ShoppingCart shoppingCart = item.getShoppingCart();
    		shoppingCart.setAmount(shoppingCart.getAmount().subtract(item.getAmount()));
    		itemRepository.delete(item);
    	}
    }
    
}
