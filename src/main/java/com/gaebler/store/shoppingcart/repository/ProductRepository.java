package com.gaebler.store.shoppingcart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gaebler.store.shoppingcart.domain.Product;

/**
 * Spring Data JPA repository for the Product entity.
 */
public interface ProductRepository extends JpaRepository<Product,String> {

}
