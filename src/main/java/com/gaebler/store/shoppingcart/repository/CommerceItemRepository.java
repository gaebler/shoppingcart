package com.gaebler.store.shoppingcart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gaebler.store.shoppingcart.domain.CommerceItem;

/**
 * Spring Data JPA repository for the CommerceItem entity.
 */
public interface CommerceItemRepository extends JpaRepository<CommerceItem,String> {
	
	CommerceItem findByShoppingCartIdAndProductId(String shoppingCartId, String productId);

}
