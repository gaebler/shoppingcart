package com.gaebler.store.shoppingcart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gaebler.store.shoppingcart.domain.ShoppingCart;

/**
 * Spring Data JPA repository for the ShoppingCart entity.
 */
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart,String> {
	
}
