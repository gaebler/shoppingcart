package com.gaebler.store.shoppingcart.web.rest;

import java.util.Enumeration;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gaebler.store.shoppingcart.domain.CommerceItem;
import com.gaebler.store.shoppingcart.domain.ShoppingCart;
import com.gaebler.store.shoppingcart.service.ShoppingCartService;

/**
 * REST controller for managing ShoppingCart.
 */
@RestController
@RequestMapping("/api")
public class ShoppingCartResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingCartResource.class);
    
    @Autowired
    private HttpServletRequest httpRequest;
    
    @Inject
    private ShoppingCartService shoppingCartService;
    
    /**
     * GET  /shoppingcart : get the shoppingCart in session.
     *
     * @param id the id of the shoppingCart to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shoppingCart, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/shoppingcart",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShoppingCart> getShoppingCart() {
    	String shoppingCartId = getSessionId();
    	log.debug("REST request to get ShoppingCart : {}", shoppingCartId);
        ShoppingCart shoppingCart = shoppingCartService.getShoppingCart(shoppingCartId);
        return new ResponseEntity<>(shoppingCart, HttpStatus.OK);
    }
    
    /**
     * POST  /shoppingcart/items : Create a new commerceItem.
     *
     * @param commerceItem the commerceItem to create
     * @return the ResponseEntity with status 200 (OK) and with body the new commerceItem
     */
    @RequestMapping(value = "/shoppingcart/items",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommerceItem> createCommerceItem(@RequestParam(value="product_id") String productId, 
    													   @RequestParam(value="quantity") Integer quantity) {
    	if(StringUtils.isEmpty(productId)||quantity==null){
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    	}
    	String shoppingCartId = getSessionId();
        CommerceItem item = shoppingCartService.addItem(shoppingCartId, productId, quantity);
        return new ResponseEntity<>(item, HttpStatus.OK);
    }
    
    /**
     * DELETE  /commerce-items/:id : delete the "id" commerceItem.
     *
     * @param id the id of the commerceItem to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/shoppingcart/items/{commerceItemId}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteCommerceItem(@PathVariable String commerceItemId) {
        log.debug("REST request to delete CommerceItem : {}", commerceItemId);
        shoppingCartService.removeItem(commerceItemId);
        return ResponseEntity.ok().build();
    }
    
    private String getSessionId() {
    	String sessionId = null;
    	Enumeration<String> headerNames = httpRequest.getHeaderNames();
    	while(headerNames.hasMoreElements()){
    		String name = headerNames.nextElement();
    		if("hazelcast.sessionId".equalsIgnoreCase(name)){
    			sessionId = httpRequest.getHeader(name);
    			break;
    		}
    	}
    	if(StringUtils.isEmpty(sessionId)){
    		sessionId = httpRequest.getSession().getId();
    	}
    	return sessionId;
    }

}
